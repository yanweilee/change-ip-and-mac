@echo off
title 网卡IP和MAC地址修改脚本
color 0A
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
if '%errorlevel%' NEQ '0' (
goto UACPrompt
) else ( goto gotAdmin )
:UACPrompt
echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"
"%temp%\getadmin.vbs"
exit /B
:gotAdmin
if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )
color 0A
echo ========================
echo 提示：
echo.
echo 有以下几个参数需要提前设置,本脚本需要管理员权限运行：
echo.
echo.一、固定注册表分支项名
echo.
echo.1.到regedit 搜索{4d36e972-e325-11ce-bfc1-08002be10318}，
echo.在节点中找DriverDesc的值是对应的网卡的节点值就是BranchID
echo.
echo.二、更新新、旧MAC地址,设备号，连接名称、新的ip地址信息
set BranchID=0002
set connectName=以太网
set oldMAC=6C02E07B14F9
set newMAC=A4BB6DE43D7B
set newAddr=192.168.51.13
echo.
echo. 输入 1 修改本机MAC地址
echo.
echo. 输入 2 恢复本机MAC地址
set /p ID=
if "%id%"=="1" set "MACStr=%newMAC%"
if "%id%"=="2" set "MACStr=%oldMAC%"
echo. 
echo 将新的MAC值写入注册表
echo ========================
set "basePath=HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Class\{4D36E972-E325-11CE-BFC1-08002bE10318}\"
set "path1=%basePath%%BranchID%"
@reg add "%path1%" /v NetworkAddress /t REG_SZ /d "%MACSTR%" /f
set "path2=%path1%\Ndi\params\NetworkAddress"
@reg add  "%path2%" /v default /t REG_SZ /d "%MACSTR%" /f
@reg add "%path2%" /v ParamDesc /t REG_SZ /d MAC-Address /f
@reg add "%path2%" /v Optional /t REG_SZ /d 1 /f
echo ========================
echo.
echo 修改ip
echo==========================
if "%id%"=="1" netsh interface ip set address name=%connectName% source=static addr=%newAddr% mask=255.255.255.0 gwmetric=1
if "%id%"=="2" netsh interface ip set address name=%connectName% source=dhcp 
echo.
echo.正在禁用本机网卡
echo.
netsh interface set interface name=%connectName% admin=DISABLED
echo 正在启用本机网卡
echo.
echo 友情提示：此操作时间较长，请耐心等待，脚本执行完成后，本窗口会自动退出。
echo.
netsh interface set interface name=%connectName% admin=ENABLED
echo.
echo 结束
@pause


