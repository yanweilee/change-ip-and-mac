# changeIPAndMAC

#### 介绍
修改ip及mac地址批处理脚本

#### 软件架构
软件架构说明


#### 安装教程

保存在ANSI编码的bat脚本，windows下双击直接执行

#### 使用说明

有以下几个参数需要提前设置：

1. 到regedit 搜索{4d36e972-e325-11ce-bfc1-08002be10318}，在节点中找DriverDesc的值是对应的网卡的节点值就是BranchID
2. 链接名称，在开始>>设置>>更改适配器选项，查看正在使用的链接名称，就是connectName
3. 设置新的ip地址
4. 设置新、旧mac地址，旧mac地址可以通过cmd下通过ipconfig /all 查看

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
